# Child POM Project

```
To Inherit dependency, properties, plugins from another pom.
We just need to change the <parent> tag of this project as

 <parent>
        <groupId>com.example</groupId>
        <artifactId>common-pom</artifactId>
        <version>0.0.1</version>
 </parent> 
 
Once we chnge the <parent> as shown above, this project will inherit all 
- dependency
- properties
- plugin etc

from specified parent. 
```

## Pre requisite

```
The parent pom must be present either in
 - one of the repository
 - local repo 
 
i.e if maven is not able to locate parent, then clone paent maven project in local 
and do "mvn clean install" in parent pom project. THIS SHOULD RESLOVE THE PROBLEM
```