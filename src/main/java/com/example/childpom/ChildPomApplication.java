package com.example.childpom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChildPomApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChildPomApplication.class, args);
    }

}
